% function prx = tworay(hbs, hrx, fo, v, r, l, ptx, grx, gtx, t)
function prx = tworay(hbs,hrx,fo,v,r,l,ptx,grx,gtx,t)
d = zeros(1, v*t);
dlos = zeros(1, v*t);
dref = zeros(1, v*t);
phi = zeros(1, v*t);
signal = zeros(1, v*t);
glos = 0;
gref = 0;
lambda = 0;

d = l:0.1:(v*t);


glos = gtx*grx;
gref = glos;
dlos = sqrt(d.^2 + ((hbs-hrx)^2));
dref = sqrt(d.^2 + ((hbs+hrx)^2))
lambda = (300000000/fo);
phi = (2*pi)*(dlos - dref)/lambda;

signal = lambda/(4*pi)*(sqrt(glos)./dlos + r*sqrt(gref)./dref.*exp(1i*phi));
prx = ptx*abs(signal).^2;

end

